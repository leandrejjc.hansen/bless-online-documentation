.. _doc_underground_prison_of_balmont:

Underground Prison of Balmont [Lv.13]
=====================================

.. figure:: img/underground_prison_of_balmont_layout.jpg
    :width: 300px
    :align: center


This is the first dungeon on the Hieron side. It teaches you about traps, telegraphs, 
environmental hazards and side objectives.

**Requirements:**

- Level: 13
- Equipment Score: 1655

.. note:: If you don't have enough Equipment Score you can buy some gear from the Towns Equipment Merchant

Bosses
++++++

First Boss
----------

.. figure:: img/underground_prison_of_balmont_boss1.jpg
    :width: 150px
    :align: center

Really straightforward fight, just try to step out of the way of the cone telegraphed attacks.

.. warning:: Pay attention to the traps on the floor near the side of the arena

Second Boss
-----------

.. figure:: img/underground_prison_of_balmont_boss2.jpg
    :width: 200px
    :align: center

The boss going to use a circle telegraphed attack around him frequently, try to avoid them.
Also he's going to spawn adds, kill them as soon as possible.

Third Boss
----------

.. figure:: img/underground_prison_of_balmont_boss3.jpg
    :width: 200px
    :align: center

Once again there are going to be adds, try to kill them as soon as possible.
When the boss spits on the floor it leaves behind a poison cloud, make sure not to stand in it.

Drops
+++++

Lv 11 and Lv 19 Green Weapons

Lv 14 Blue Armours
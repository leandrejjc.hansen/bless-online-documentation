.. _doc_formatting_guide_lines:

Formatting Guide Lines
=======================

We use **reStructuredText** here to format the docs. What is that? "reStructuredText is an easy-to-read, what-you-see-is-what-you-get 
plaintext markup syntax and parser system", you can easily learn the syntaxes from the 
`Official reStructuredText Documentation <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`_, 
the `Sphinx documentation <http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_ or from 
`Thomas Cokelaer's guide <https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html#images-and-figures>`_ (probably the most user friendly).
On top of these you can also also find some commonly used syntaxes down below.

Text Manipulation
++++++++++++++++++

*Italic* - ``*Italic*``

**Bold** - ``**Bold**``

``Code`` - ````Code````

`URL alias <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`_ - ```URL alias <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`_``

Sections - Just put ``=``, ``+``, ``-`` or ``^`` under the text until the symbols are longer than the text 

Images/Videos
++++++++++++++

**Images** mostly used for low resolution pictures, when you don't need captions and it's not important to enlarge the image by clicking on it

**Figures** used when you want to display an enlargeable picture or you need captions

**Video** it's a bit complex code the a nice responsive design, but all you need to do is copy the code and change the video URL. 
(You can find the Embed URL of a video by clicking Share-> Embed on a YouTube video)

.. tabs::

    .. code-tab:: rst Image

        .. image:: img/getting_started_edit_on_gitlab.jpg
    
    .. code-tab:: rst Figure

        .. figure:: img/getting_started_fork.jpg
            :width: 100%
            :align: center
            
            Optional caption

    .. code-tab:: rst Video

        .. raw:: html

            <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
                <iframe src="https://www.youtube.com/embed/_N2L5e9_bK8" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
            </div>

.. note::

    You will need to add the images you'd like to use to the "img" folder next to the file.

References
++++++++++

Creation
---------

You can create a reference with the syntax: ``.. referencename:``

.. important::

    Please read :doc:`naming_conventions` before creating a reference

Usage
------

Referencing a document :doc:`../../guides/dungeons/underground_prison_of_balmont`: ``:doc:`../../guides/dungeons/underground_prison_of_balmont```

Referencing a previously made reference point with :ref:`custom text <doc_formatting_guide_lines>`: 
``:ref:`custom text <doc_formatting_guide_lines>`:``

Tabs
++++

You can read more about the usage of tabs on its `Github <https://github.com/djungelorm/sphinx-tabs>`_.

.. tabs::

    .. code-tab:: rst Basic

       .. tabs::

            .. tab:: Label

                Content you want to show

    .. code-tab:: rst Code

       .. tabs::

            .. code-tab:: language Label

                Content you want to show

    .. code-tab:: rst Grouped

       .. tabs::

            .. group-tab:: Label

                Content you want to show
        
        .. tabs::

            .. group-tab:: Label

                Content you want to show




Admonitions
+++++++++++

You can read more about admonitions at David Goodger's `guide <http://docutils.sourceforge.net/docs/ref/rst/directives.html#admonitions>`_

.. note::

    .. code-block:: rst

        .. note::

            Text

.. important::

    .. code-block:: rst
    
        .. note::

            Text

.. warning::

    .. code-block:: rst
    
        .. note::

            Text

Creating Documents
+++++++++++++++++++

When you create a document you are going to need to include that documents in a "doctree".
You can do this by opening the ``index.rst`` file next to your document and adding the name of your document to
it's doctree. Please before creating a new document also read :doc:`naming_conventions`:!

Creating Folders
++++++++++++++++++

When creating a folder it's important that you include the index file's location in the root directorys ``index.rst``
doctree. If you want to create a new category you'll need to create a new doctree, if you want only a new
subcategory you'll have to include the index files location in an existing doctree.
Please before creating a new folder also read :doc:`naming_conventions`:!